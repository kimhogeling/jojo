<?php

function getDirectoryList($directory) {
	$results = array();
	$handler = opendir($directory);
	while ($file = readdir($handler)) {
		if ($file != "." && $file != "..") {
			$results[] = $file;
		}
	}
	closedir($handler);
	return $results;
}

include 'config.php';
include 'modules/dibi/dibi.php';
dibi::connect($config['database']);
$result = dibi::query('SELECT photo FROM themes ORDER BY photo ASC');
$UsedImages = $result->fetchAll();
$used = array();
foreach ($UsedImages as $photos) {
	$used[] = $photos["photo"];
}
$files = getDirectoryList("media/images/themes");
sort($files);
$total = 0;
$found = 0;
$notfound = 0;
$output = "<h6>searching for files:</h6>";
foreach ($files as $file) {
	$total++;
	if (in_array($file, $used)) {
		$output .= '<div style="background-color: #afa;">Used: ' . $file . '</div>';
		$found++;
	} else {
		$output .= '<div style="background-color: #faa;">Old: ' . $file . '</div>';
		$notfound++;
	}
}
echo $output . "<br />Total: $total <br />Used: $found <br /> Old: $notfound";
?>
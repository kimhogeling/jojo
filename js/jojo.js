document.onkeyup = function(e) {
    
    var key = e.which;
    if (key === 37 || key === 39) {
        var link;

        if (key === 39) {
            link = document.getElementById("linkright");
        } else {
            if (key === 37) {
                link = document.getElementById("linkleft");
            }
        }

        if (typeof link === 'object') {
            if (link !== null) {
                document.location.href = link.href;
            }
        }
    }

};

document.onclick = function (e) {
    var element = e.srcElement || e.target;
    if (element.id && element.id === 'shownotes') {
        element.className = 'hide';
        document.getElementById('logo').className = 'hide';
        document.getElementById('noteswrapper').className = 'show';
    }
}

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-29873885-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

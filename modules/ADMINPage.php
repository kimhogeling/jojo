<?php

class ADMINPage {

    private $html;

    public function __construct($page = "home", $title = "home") {
        $this->head($page, $title);
        $this->openPage();
    }

    private function head($page = "home", $title = "home") {
        require 'config.php';
        foreach ($config["activemodules"] as $module) {
            $filename = 'modules/' . $module . '.php';
            require $filename;
        }

        $lang = 'NL';

        if (isset($_GET['lang'])) {
            if ($_GET['lang'] == "DE") {
                $lang = 'DE';
            }
        } elseif (stripos(strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), "de") === 0) {
            $lang = 'DE';
        }

        $html = new html($lang);
        $html->html(true);
        $html->head(true);
        $html->favicon('admin');
        $html->cssGoogleWebFont("Acme");
        $html->charset("utf8");
        $html->title("JOJO Beheer met kaas");

        foreach ($config["admincss"] as $css) {
            $cssfile = 'css/' . $css . '.css';
            if (file_exists($cssfile)) {
                $html->csslink($cssfile);
            } else {
                echo $cssfile . " does not exist! Cannot load style!";
                die();
            }
        }

        foreach ($config["adminjs"] as $js) {
            $jsfile = 'js/' . $js . '.js';
            if (file_exists($jsfile)) {
                $html->jslink($jsfile);
            } else {
                echo $jsfile . " does not exist! Cannot load style!";
                die();
            }
        }
        $html->head(false);
        $this->html = $html;
    }

    private function openPage() {
        $html = $this->html;
        $html->body(true);
        $html->mainwrapper(true);
        $html->adminTop();
        $html->logout();
        $html->middle(true);
    }

    public function draw($pageName = "home") {
        if (method_exists($this, "draw" . $pageName)) {
            $pageName = "draw" . $pageName;
            $this->$pageName();
        } else {
            die("don't know method: " . $pageName());
        }
        $this->closePage();
    }

    public function drawStart() {
        $html = $this->html;
        $html->adminStart();
    }

    public function drawThemes() {
        $html = $this->html;

        if (isset($_GET["new"])) {

            $result = dibi::query("SELECT MAX(sequence) FROM themes");
            $newSequence = $result->fetchSingle() + 1;

            $newTheme = array(
                'active' => 0,
                'sequence' => $newSequence,
            );
            dibi::query('INSERT INTO `themes`', $newTheme);
            header('Location: admin.php?page=Themes');
        }
        $id = $this->checkID();
        if ($id) {
            switch ($id["action"]) {
                case "edit":
                    $this->saveTheme($id["id"]);
                    $html->editTheme($id["id"]);
                    return;
                    break;

                case "delete":
                    dibi::query("DELETE FROM themes WHERE id=%i", $id["id"]);
                    break;

                case "turnon":
                    dibi::query('UPDATE themes SET active=1 WHERE id=%i', $id["id"]);
                    break;

                case "turnoff":
                    dibi::query('UPDATE themes SET active=0 WHERE id=%i', $id["id"]);
                    break;

                case "up":
                    dibi::query("
                        UPDATE
                                themes as above
                        LEFT JOIN themes as current ON (current.sequence = (above.sequence + 1))
                        SET
                                above.sequence = current.sequence
                        WHERE
                                current.`id`=%i
                        "    , $id["id"]
                    );

                    dibi::query("
                        UPDATE
                                themes
                        SET
                                sequence = sequence - 1
                        WHERE
                                id=%i
                                "    , $id["id"]
                    );
                    break;

                case "down":
                    dibi::query("
                        UPDATE
                                themes as below
                        LEFT JOIN themes as current ON (current.sequence = (below.sequence - 1))
                        SET
                                below.sequence = current.sequence
                        WHERE
                                current.`id`=%i
                        "    , $id["id"]
                    );

                    dibi::query("
                        UPDATE
                                themes
                        SET
                                sequence = sequence + 1
                        WHERE
                                id=%i
                                "    , $id["id"]
                    );
                    break;

                default:
                    break;
            }
        }
        $html->adminThemes();
    }

    private function saveTheme($id) {
        if ($_FILES["file"]) {
            if ($_FILES["file"]["error"] > 0) {
                $errors = array(
                    1 => '1',
                    2 => '2',
                    3 => '3',
                    4 => '4',
                    5 => '5',
                );
                echo "ERROR = " . $errors[$_FILES["file"]["error"]];
            } else {
                if ($_FILES["file"]["type"] == "image/jpeg" || strtolower(substr($_FILES["file"]["name"], strlen($_FILES["file"]["name"]) - 3, 3)) == "jpg") {
                    $newfilename = md5($_FILES["file"]["name"] . microtime()) . ".jpg";
                    $newfullfile = "media/images/themes/" . $newfilename;
                    if (file_exists($newfullfile)) {
                        echo "ERROR: " . $newfilename . " bestaat al!!<br /> ";
                    } else {
                        move_uploaded_file($_FILES["file"]["tmp_name"], $newfullfile);
                    }
                    if (file_exists($newfullfile)) {
                        $src_img = imagecreatefromjpeg($newfullfile);
                        $oh = imagesy($src_img);
                        $ow = imagesx($src_img);
                        $dst_img = imagecreatetruecolor(200, 130);
                        if (imagecopyresized($dst_img, $src_img, 0, 0, 0, 0, 200, 130, $ow, $oh)) {
                            imagejpeg($dst_img, "media/images/themes/small_" . $newfilename);
                        } else {
                            echo "ERROR: De kleine versie kon niet worden gemaakt!<br />";
                        }
                        $arr = array(
                            photo => $newfilename,
                        );
                        dibi::query('UPDATE themes SET ', $arr, ' WHERE id=%i', $id);
                    } else {
                        echo "ERROR: Het bestand kon om één of andere technische reden niet worden opgeslagen!<br />";
                    }
                } else {
                    echo "ERROR: Je mag alléén jpg bestanden uploaden!<br />";
                }
            }
        }

        if (isset($_POST["nameNL"]) && isset($_POST["nameDE"]) && isset($_POST["descNL"]) && isset($_POST["descDE"])) {
            $values = array(
                'nameNL' => $_POST["nameNL"],
                'nameDE' => $_POST["nameDE"],
                'descriptionNL' => $_POST["descNL"],
                'descriptionDE' => $_POST["descDE"],
            );
            dibi::query('UPDATE `themes` SET ', $values, 'WHERE `id`=%i', $id);
            header('Location: admin.php?page=Themes');
        }
    }

    public function drawPricesandconditions() {
        $html = $this->html;
        $this->savePrices();
        $html->adminPrices();
    }

    private function savePrices() {
        if (isset($_POST["pricelistNL"]) && isset($_POST["pricelistDE"])) {
            $values = array(
                'translation' => $_POST["pricelistNL"],
            );
            dibi::query('UPDATE `i18n` SET ', $values, 'WHERE %and', array(
                array("`countrycode` = %s", 'NL'),
                array("`key` = %s", 'pricelisttext'),
            )
            );
            $values = array(
                'translation' => $_POST["pricelistDE"],
            );
            dibi::query('UPDATE `i18n` SET ', $values, 'WHERE %and', array(
                array("`countrycode` = %s", 'DE'),
                array("`key` = %s", 'pricelisttext'),
            )
            );
        }
    }

    public function drawEvents() {
        $html = $this->html;
        $this->saveEvents();
        $html->adminEvents();
    }

    private function saveEvents() {
        if (isset($_POST["eventsNL"]) && isset($_POST["eventsDE"])) {
            $values = array(
                'translation' => $_POST["eventsNL"],
            );
            dibi::query('UPDATE `i18n` SET ', $values, 'WHERE %and', array(
                array("`countrycode` = %s", 'NL'),
                array("`key` = %s", 'eventstext'),
            )
            );
            $values = array(
                'translation' => $_POST["eventsDE"],
            );
            dibi::query('UPDATE `i18n` SET ', $values, 'WHERE %and', array(
                array("`countrycode` = %s", 'DE'),
                array("`key` = %s", 'eventstext'),
            )
            );
        }
    }

    private function closePage() {
        $html = $this->html;
        $html->middle(false);
        $html->mainwrapper(false);
        $html->body(false);
        $html->html(false);
        $html->deploy();
    }

    private function checkID() {
        $checks = array("edit", "delete", "turnon", "turnoff", "up", "down");
        foreach ($checks as $action) {
            if ($this->checkURLForID($action)) {
                return array(
                    "action" => $action,
                    "id" => $_GET[$action],
                );
            }
        }
        return false;
    }

    private function checkURLForID($action) {
        if (isset($_GET[$action])) {
            if (filter_var($_GET[$action], FILTER_VALIDATE_INT)) {
                return true;
            }
        }
        return false;
    }

}

?>
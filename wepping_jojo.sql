-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 28. Mrz 2015 um 15:47
-- Server Version: 5.1.59
-- PHP-Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `wepping_jojo`
--

-- --------------------------------------------------------
CREATE DATABASE wepping_jojo;
USE wepping_jojo;
--
-- Tabellenstruktur für Tabelle `i18n`
--

DROP TABLE IF EXISTS `i18n`;
CREATE TABLE IF NOT EXISTS `i18n` (
  `key` varchar(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `translation` varchar(5000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `countrycode` varchar(2) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `i18n`
--

INSERT INTO `i18n` (`key`, `translation`, `countrycode`) VALUES
('contact', '<a href="http://goo.gl/maps/5muOK" target="_blank">Dorpsstraat 129a<br>9699 PG Vriescheloo<br>Nederland</a><br>0597 855 447 / 06 589 409 31<br>jojofeestwinkel@hotmail.com', 'NL'),
('contact', '<a href="http://goo.gl/maps/tMEzG" target="_blank">Dorpsstraat 129a<br>9699 PG Vriescheloo<br>Niederlande</a><br>+31597855447 / +31658940931<br>jojofeestwinkel@hotmail.com', 'DE'),
('pricelisttext', '<h2>Verhuurprijzen</h2>\r\n<table>\r\n  <tr>\r\n    <td>Kleding kinderen</td>\r\n    <td>vanaf € 6,00</td>\r\n    <td>borg  € 10,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Kleding volwassenen</td>\r\n    <td>vanaf € 10,00</td>\r\n    <td>borg  € 15,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Maskers</td>\r\n    <td>vanaf € 4,00</td>\r\n    <td>borg  € 10,00 tot 15,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Pruiken / Hoofddeksels</td>\r\n    <td>vanaf € 1,50</td>\r\n    <td>borg   € 5,00</td>\r\n  </tr>\r\n</table>\r\n<br>\r\nVoor toneelverenigingen en scholen hebben wij aantrekkelijke kortingen<br>\r\n<br>\r\n\r\n<h2>Voorwaarden</h2>\r\n<br>\r\nHuur van kostuums en accessoires gelden voor 1 periode, tenzij anders overeengekomen<br>\r\n<br>\r\n1 periode = 3 dagen<br>\r\n1e dag: ophalen<br>\r\n2e dag: gebruiken<br>\r\n3e dag: retour<br>\r\n<br>\r\n</td><br>\r\n<br> \r\nDe verhuurkosten en de borg dienen bij ophalen te worden betaald\r\nBij kleding retour, krijgt u de borg weer terug<br>\r\n<br>\r\n<td>Graag de kleding ongewassen terug brengen<br>\r\n<br>\r\n</td><br>\r\n<br>U kunt bij JOJO niet pinnen<br>', 'NL'),
('pricelisttext', '<h2>Mietpreise</h2>\r\n<table>\r\n  <tr>\r\n    <td>Kostüme für Kinder</td>\r\n    <td>ab € 6,00</td>\r\n    <td>Kaution € 10,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Kostüme für Erwachsene</td>\r\n    <td>ab € 10,00</td>\r\n    <td>Kaution € 10,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Masken</td>\r\n    <td>ab € 4,00</td>\r\n    <td>Kaution € 10,00</td>\r\n  </tr>\r\n  <tr>\r\n    <td>Perücken / Kopfbedeckung</td>\r\n    <td>ab € 1,50</td>\r\n    <td>Kaution € 5,00</td>\r\n  </tr>\r\n</table>\r\n<br>\r\nFür Theatergruppen und Schulen haben wir attraktive Rabbate.<br>\r\n<br>\r\nDie Vermietung von Kostümen und Zubehör läuft, soweit nicht anders vereinbart, über den Zeitraum einer Periode.<br>\r\n1 Periode = 3 Tage<br>\r\n1. Tag: Ausleihe<br>\r\n2. Tag: Verwendung<br>\r\n3. Tag: Rückgabe<br>\r\n<br>\r\n\r\n<br>\r\n<br>\r\nDie Mietkosten und Kaution sind am Tag (Tag 1) der Ausleihe zu leisten.<br>\r\nBei Rückgabe erhalten Sie Ihre Kaution zurück.<br>\r\n<br>\r\nBringen Sie die Kostüme bitte ungewaschen zurück.', 'DE'),
('eventstext', '<h2>Griezeltochten georganiseerd door feestbedrijf JOJO</h2>\r\nGriezeltochten met vragen, opdrachten, scenes en schrikpunten.<br>\r\nOp zaterdag avond vanaf 20:00 (wintertijd) en vanaf 22:30 (zomertijd).<br>\r\nKinderen jonger dan 16 jaar met goedkeuring van de ouders/voogd.<br>\r\nDe griezeltochten zijn inclusief koffie/thee met cake en bij terugkomst broodje worst of broodje vegetarisch, en consumptie.<br>\r\nNa afloop, als alle groepen weer terug zijn, is er gelegenheid voor een feestje met \r\ngezellige muziek onder het genot van een hapje en drankje.<br>\r\n<br>\r\n<hr>\r\n<br>\r\n<h2>Horrortochten georganiseerd door feestbedrijf JOJO</h2>\r\nHorrortochten met vragen en opdrachten maar met veel engere, bloederige scenes en \r\ntaferelen dan tijdens een griezeltocht.<br>\r\nOp zaterdag avond vanaf 21:00 (alleen in de wintertijd).<br>\r\nLeeftijd minimaal 16 jaar.<br>\r\nInclusief koffie/thee met cake en bij terugkomst broodje worst of broodje vegetarisch, en consumptie.<br>\r\nNa afloop, als alle groepen (hopelijk) weer terug zijn, is er de gelegenheid voor een feestje met gezellige muziek onder het genot van een hapje en drankje.<br>\r\n<br>\r\n<h3>Prijzen griezeltochten/horrortochten 2013</h3>\r\n<table>\r\n<tbody><tr><th>aantal personen</th><th>excl. BTW</th><th>incl. BTW</th></tr>\r\n<tr><td>groep t/m 25 personen</td><td>€ 326.45</td><td>€ 395.00</td></tr>\r\n<tr><td>26 t/m 35 personen</td><td>€ 12.60 p/p</td><td>€ 15.25 p/p</td></tr>\r\n<tr><td>36 t/m 45 personen</td><td>€ 12.15 p/p</td><td>€ 14.70 p/p</td></tr>\r\n<tr><td>46 t/m 55 personen</td><td>€ 11.69 p/p</td><td>€ 14.15 p/p</td></tr>\r\n<tr><td>56 t/m 65 personen</td><td>€ 11.24 p/p</td><td>€ 13.60 p/p</td></tr>\r\n<tr><td>66 t/m 75 personen</td><td>€ 10.58 p/p</td><td>€ 12.80 p/p</td></tr>\r\n</tbody></table><br>\r\n<hr>\r\n<br>\r\n<h2>Kinderfeestje Heksenschool</h2>\r\nDe kinderen worden ontvangen op de Heksenschool,<br>\r\nwaar ze worden aangekleed als leerling heks of tovenaar.<br>\r\nDe heksenleerlingen ontmoeten de grote heks en Joris, die al 2 jaar op de heksenschool zit..<br>\r\nEen middag met spel, spannende speurtocht, knutselen en nog veel meer.<br>\r\n<br>\r\n<hr>\r\n<br>\r\n<h2>Kinderfeestje Piraten</h2>\r\nDe kinderen worden verkleed als piraat en beleven een groot avontuur tijdens een speurtocht..<br>\r\n<br>\r\n<hr>\r\n<br>\r\nVoor vragen of meer informatie bel 0597 855447 of stuur een mailtje naar <a href="mailto:jojofeestwinkel@hotmail.com">jojofeestwinkel@hotmail.com</a>', 'NL'),
('eventstext', '<h2>Kinderpartie Hexenschule</h2>\r\nDie Kinder werden auf der Hexenschule empfangen,\r\nwo Sie sich wie Hexen oder Zauberer verkleiden.<br>\r\n<br>\r\nDie Hexenneulinge lernen die große Hexe und Joris, der schon seit 2 Jahre auf der Hexenschule ist, kennen.<br>\r\n<br>\r\nEin Tag mit Spiele, aufregender Suche, basteln und noch viel mehr<br>\r\n<br>\r\n<hr>\r\n<br>\r\n<h2>Kinderpartie Piraten</h2>\r\nDie Kinder werden wie Piraten verkleidet und erleben ein großes Abenteuer während der Suche im Piratenwald.<br>\r\n<br>\r\n<hr>\r\n<br>\r\n<h2>Gruseltour</h2>\r\nDie Gruseltour beinhaltet Fragen, Aufgaben und gruselige Szenen.<br>\r\nSamstagsabends ab 20:00 Uhr (in Wintermonaten) und 22:30 Uhr (in Sommermonaten).<br>\r\n<br>\r\nKinder, die jünger als 16 Jahre sind, benötigen die Einwilligung ihrer Eltern bzw. des Erziehungsberechtigten.<br>\r\n<br>\r\nDie Gruseltour beinhaltet Tee bzw. Kaffee und Kuchen und nach dem schaurigen Vergnügen, werden Sie durch ein leckeres Brot wieder zum Leben erweckt.<br>\r\n<br>\r\nFalls Ihnen, nachdem alle zurückgekehrt sind, noch zum Feiern zumute ist, begrüßen wir Sie hezlich mit Getränken und guter Musik den Abend ausklingen zulassen.<br>\r\n<br>\r\n<h2>Horrortour</h2>\r\n<br>\r\nDie Horrortour ist, wie die Gruseltour mit Fragen und Aufgaben bestückt, doch die Szenen sind deutlich gruselliger und blutiger (NICHTS FÜR SCHWACHE NERVEN!).<br>\r\n<br>\r\nSamstagsabends ab 21:00, lediglich in den Wintermonaten.<br>\r\n<br>\r\nSie müssen mindestens 16 Jahre alt sein.<br>\r\n<br>\r\nDie Horrortour beinhaltet ebenfalls Tee/Kaffee und Kuchen und nach dem schaurigen Vergnügen, werden Sie durch ein leckeres Brot wieder zum Leben erweckt.<br>\r\n<br>\r\nFalls Ihnen, nachdem alle zurückgekehrt sind, noch zum Feiern zumute ist, begrüßen wir Sie hezlich mit Getränken und guter Musik den Abend ausklingen zulassen.<br>\r\n<br>\r\n<h3>Preise für Horror- bzw. Gruseltour 2013</h3>\r\n<table>\r\n<tbody><tr><th>Personenanzahl</th><th>exkl. MWST</th><th>inkl. MWST</th></tr>\r\n<tr><td>Gruppen mit 25 Personen</td><td>€ 326.45</td><td>€ 395.00</td></tr>\r\n<tr><td>26 mit bis zu 35 Personen</td><td>€ 12.60 p/P</td><td>€ 15.25 p/P</td></tr>\r\n<tr><td>36 mit bis zu  45 Personen</td><td>€ 12.15 p/P</td><td>€ 14.70 p/P</td></tr>\r\n<tr><td>46 mit bis zu 55 Personen</td><td>€ 11.69 p/P</td><td>€ 14.15 p/P</td></tr>\r\n<tr><td>56 mit bis zu  65 Personen</td><td>€ 11.24 p/P</td><td>€ 13.60 p/P</td></tr>\r\n<tr><td>66 mit bis zu 75 Personen</td><td>€ 10.58 p/P</td><td>€ 12.80 p/P</td></tr>\r\n</tbody></table><br>\r\n<hr>\r\n<br>\r\n<br>\r\n<hr>\r\n<br>\r\nFür Fragen und mehr Informationen rufen Sie uns an unter 0031 597 855447 oder senden Sie eine Mail an <a href="mailto:jojofeestwinkel@hotmail.com">jojofeestwinkel@hotmail.com</a>', 'DE'),
('pricelisttitle', 'Preisliste & Verleihbedingungen', 'DE'),
('pricelisttitle', 'Prijslijst & Verhuurcondities', 'NL'),
('eventstitle', 'JOJO activiteiten', 'NL'),
('eventstitle', 'Auch für Kinderparties sind Sie bei uns richtig', 'DE'),
('opened', '<table><tbody><tr><td>Ma - Vr</td><td>10:00 - 20:00</td></tr><tr><td style="color:red">Dinsdag</td><td><span style="color:red">13:00</span> - 20:00</td></tr><tr><td>Zaterdag</td><td>10:00 - 18:00</td></tr><tr><td>Zondag</td><td>Gesloten</td></tr></tbody></table>', 'NL'),
('opened', '<table><tbody><tr><td>Mo - Fr</td><td>10:00 - 20:00</td></tr><tr><td style="color:red">Dienstag</td><td><span style="color:red">13:00</span> - 20:00</td></tr><tr><td>Samstag</td><td>10:00 - 18:00</td></tr><tr><td>Sonntag</td><td>Geschlossen</td></tr></tbody></table>', 'DE'),
('directionstitle', 'Route', 'NL'),
('directionstitle', 'Wegbeschreibung', 'DE'),
('directionstext', '<img src="media/JOJO_Vriescheloo_Route.png" alt="Route">', 'NL'),
('directionstext', '<img src="media/JOJO_Kostuemeverleih_Wegbeschreibung.png" alt="Wegbeschreibung">', 'DE');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `themes`
--

DROP TABLE IF EXISTS `themes`;
CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(60) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT 'stiftungwarentest.gif',
  `nameNL` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'nieuw thema',
  `nameDE` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'neues Thema',
  `descriptionNL` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'Nederlandse beschrijving',
  `descriptionDE` varchar(2000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 'Duitse beschrijving',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `sequence` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=154 ;

--
-- Daten für Tabelle `themes`
--

INSERT INTO `themes` (`id`, `photo`, `nameNL`, `nameDE`, `descriptionNL`, `descriptionDE`, `active`, `sequence`) VALUES
(41, '73722d768547ddc8e5a447bebfbb071b.jpg', 'BEROEPEN', 'BERUFE', 'ZUSTER - DOKTER - O.K.', 'SCHWESTER - ARZT - O.K.', 1, 56),
(42, '4ff4be31fc03f0ee46a5113cba30f429.jpg', 'FUN', 'SPAß', 'BLIKSEM KOSTUUM MAN - BLIKSEM KOSTUUM VROUW - ZANGER - PRESENTATOR', 'BLITZ MANN - BLITZ FRAU - SÂNGER - MODERATOR', 1, 54),
(43, 'bf76989ab99322e5edd6930574105adb.jpg', 'DIEREN', 'TIERE', 'OLIFANT - BIJEN KOSTUUM MAJA EN WILLIE DE BIJ', 'ELEFANT - DIE BIENE MAJA UND WILLI ', 1, 46),
(44, '848d20f046277eddd2e2aa85ac0e079b.jpg', 'HELDEN', 'HELDEN', 'LEGOLAS - ELF - LORD OF THE RINGS', 'LEGOLAS - ELF - HERR DER RINGE', 1, 44),
(45, '229d27cb1bba214b5d69dcdc90454f94.jpg', 'SPROOKJES', 'MÂRCHEN', 'FEE - NEPTUNUS - ZEEMEERMIN ', 'FEE - NEPTUN - MEERJUNGFRAU', 1, 43),
(46, '3acfe016a05281ccd3c5870b3738ec85.jpg', 'LANDEN', 'LÄNDER', 'SPAANS SIGNORITA SPAANSE VROUW - SPAANS MATADOR SPAANSE MAN - SIGNORITA SPAANSE VROUW - MEXICAAN ', 'SPANISCHE SIGNORITA SPANISCHE FRAU - SPANISCHER MANN - SPANISCHE SIGNORITA SPANISCHE FRAU - MEXIKANER', 1, 45),
(48, 'c137ba2ee98e8593a92801bf4f92b70b.jpg', 'LANDEN', 'LÄNDER', 'Tiroler kleding - Zigeuner ', 'Tiroler Kostüme - Zigeuner', 1, 16),
(49, '9c976a9bf1e5ca02c64abb2ee6c708aa.jpg', 'DIEREN', 'TIERE', 'WINNIE DE POOH - KOE MAN - TIJGERTJE', 'WINNIE PUH - KUH MANN - TIGER', 1, 41),
(50, 'd2cc7fbba910c36e37dc48562e92a663.jpg', 'GRIEZEL', 'GRAUEN', 'SCREAM - ALIEN BUITENAARDSWEZEN - SKELET', 'SCREAM - ALIEN AUßERIRDISCHE - SKELETT', 1, 40),
(51, '2fbddf6a16563685fc791de5523dbea2.jpg', 'FUN', 'SPAß', 'CLOWN PIPO - CLOWN NAR - BIERKABOUTER - DOMPTEUR SPREEKSTALMEESTER\r\nCIRCUS', 'CLOWN PIPO - CLOWN HOFFNAR - BIERGNOM - TIERTRAINER ZIRKUSDIREKTOR\r\nZIRKUS', 1, 38),
(52, 'c70c2eb24e438a1c8e541f144a7c9628.jpg', '50, 60, en 70er jaren', '50, 60, en 70er Jahre', 'HIPPIE VROUW - KLEURIG - HIPPIE MAN', 'HIPPIE FRAU - FARBIG - HIPPIE MANN', 1, 36),
(53, 'b2fe53b479a99b3c31b12ca3dda82095.jpg', 'LANDEN', 'LÄNDER', 'CHINEZEN - SLANGENBEZWEERDER - PAPOUA´S - CHINEESJES', 'CHINESEN - SCHLANGENBESCHWÖRER - PAPOUA´S - CHINESEN', 1, 34),
(54, '845efbc01619e38797f6daab00384ef0.jpg', 'WESTERN', 'WESTERN', 'COWBOY - WESTERNLADY - COWGIRL', 'COWBOY - WESTERNLADY - COWGIRL', 1, 5),
(55, 'ec4ad9429d7877dd94e3d84d7b742832.jpg', 'BEROEPEN', 'BERUFE', 'OBER - VERPLEEGSTER - POSTBODE', 'KELLNER - KRANKENSCHWESTER - BRIEFTRÄGER', 1, 33),
(56, '8266a40214d8f0f2327454897020dfe0.jpg', 'FUN', 'SPAß', 'VERKEERSLICHT - BLOEMENMEISJE - VOETBAL', 'AMPEL - BLUMENMÄDCHEN - FUßBALL  ', 1, 30),
(57, 'a768a4800577610cf40cd10bac31a4fc.jpg', 'DIEREN', 'TIERE', 'KOE - KOE - PAASHAAS HAAS - KIP', 'KUH - KUH - OSTERHASE HASE - HUHN', 1, 32),
(58, 'ba742a813a0f2b64f7f8613c021d4223.jpg', 'FUN', 'SPAß', 'OUDERWETSE ZWEMMER - TUIN KABOUTER VROUW - ZWERVER - BOEVEN ', 'ALTMODISCH SCHWIMMER - GARTENZWERG FRAU - WANDERER - HÄFTLINGE', 1, 28),
(59, '298eb0c7eb3712dfe32b2188c027d037.jpg', 'DIEREN', 'TIERE', 'Kip - Aap - Roze Panter', ' Huhn - Affe - Pink Panter', 1, 20),
(60, '0ef4621bafd98c344b457b70d51f0b7f.jpg', 'LANDEN', 'LÄNDER', 'BEIERS - BEIERS - SCHOTSE - SCHOT', 'BAYERN - BAYERN - SCHOTTISCH - SCHOTTISCHE MÄNNER', 1, 10),
(61, '27d5cf6b552d02f300210661cda8a250.jpg', 'FUN', 'SPAß', 'Laurel en Hardy - Biermeid - Bierman - Touristen ', 'Laurel und Hardy - Bierfrau - Biermann - Touristen', 1, 27),
(62, '2b500c7c265f4ae46fe7a3dfa7c44926.jpg', 'BEROEPEN', 'BERUFE', 'STEWARDESS - PILOOT', 'STEWARDESS - PILOT', 1, 29),
(63, '99cf6fb93b4c8066b647b3e46f0d8f65.jpg', 'FUN', 'SPAß', 'VOOR MANNEN MET MEER LEF!', 'FÜR MÄNNER MIT MEHR MUT !', 1, 25),
(64, '3e1c8e9753c68fcffd2e0279ba53a576.jpg', 'GRIEZEL / HORROR TOCHTEN', 'HORROR TRIP', 'Wij bieden u een zeer spannende nachtwandeling aan in het griezelige horrorbos\r\n Voor een onvergetelijke avond \r\n\r\nVragen of informatie? Neem gerust contact met ons op: \r\n e-mail jojofeestwinkel@hotmail.com\r\n tel: 0597 855447\r\n\r\n\r\n\r\n\r\n', 'Von einer gruseligen Nachtwanderungen bis zu einem Horrortrip, den Sie nie vergessen werden\r\nWir bieten ihnen ein Abenteuer im Gruselwald\r\n\r\nKontaktieren Sie uns einfach per\r\nE-Mail: jojofeestwinkel@hotmail.com\r\ntelefon: 0031597 855447\r\n \r\n', 1, 31),
(65, 'f60ff81be818c20fa17e6e46336aeadd.jpg', 'WESTERN', 'WESTERN', 'INDIAAN - WESTERN KOSTUUM MAN - WESTERN KOSTUUM VROUW - INDIAAN / KRIJGER ', 'INDIANER - WESTERN KOSTÜME FÜR MÄNNER UND FRAUEN - INDIANER / KRIEGER', 1, 18),
(66, 'edf3e587fdf23f60e47cf5c4b9e6c2e9.jpg', 'LANDEN', 'LÄNDER', 'CLEOPATRA - FARAO - OOSTERSE PRINSES - OOSTERSE PRINS   ', 'CLEOPATRA - PHARAO - ORIENTALISCHER PRINZESSIN - ORIENTALISCHER PRINZ', 1, 1),
(67, 'e8d349d2d0057923635abbbaf605f32e.jpg', 'Glitter kostuums', 'Glitter kostume', 'Glitter jasje - Glitter jurkje - Glitter colbert \r\nGlitter party - Disco party - Disco kleding - Disco jurkje - Disco jasje - Glitter pak', 'Glitter kostume - Glitter party\r\n', 1, 4),
(68, '079409948132e6961b772eaa68640a75.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'DIT IS ALVAST EEN KLEINE GREEP UIT ONZE RUIME KEUS IN KINDERKOSTUUMS', 'Das ist schon eine kleine Auswahl aus unserer großen Auswahl Kostüme für Kinder', 1, 79),
(69, '39324d4f038f5e293d9417334d3ef89d.jpg', 'GRIEZEL', 'GRAUEN', 'funny duivel - funny dracula - duivel', 'lustige Teufel - lustig Dracula, Teufel', 1, 48),
(70, '94866c04c1393a37b046c6bb7a76006c.jpg', 'PIRATEN', 'PIRATEN', 'piraat vrouw - piraat man - piraat caribbean', 'Piraten-Frau - Piraten Mann - Piraten caribbean', 1, 42),
(71, '8368e631c44d8fe2be987d3725a3a42d.jpg', 'LANDEN', 'LÄNDER', 'Spaanse man - Spaanse dame - Spanjaard ', 'Spanisch Mann - Spanisch Dame - Spanier', 1, 39),
(96, 'beebee60e334c70b7748d7a4887e86b9.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Koningin - Ninja - Spaanse dame', 'Königin - Ninja - Spanische Dame', 1, 80),
(73, '6f13a71e51b09659bf19c7bf3126cdd7.jpg', 'SPROOKJES', 'MÂRCHEN', 'Feeën - Merlijn - Prinses Viola', 'Feen - Merlijn der Zauberer - Prinzessin Viola', 1, 50),
(95, '128d1f468a98aeac076ec4cecb4fff10.jpg', 'LANDEN', 'LÄNDER', 'Tropical vrouw - Tropical Man - Reggae', 'Tropical  - Tropical - Reggae', 1, 7),
(74, 'e4d7a27f5f941d1fa62b7ec64b587ca9.jpg', 'MIDDELEEUWEN', 'MITTELALTER', 'Mozart - Kasteeldame - Romeo', 'Mozart - Burg-dame - Romeo', 1, 57),
(75, 'ccc7522505a2c12703578cbe26ade8e6.jpg', 'MIDDELEEUWEN', 'MITTELALTER', 'Barok man - Barok dame - Componist ', 'Herr Barock - Barock-Dame - Komponist', 1, 23),
(76, 'd192e33282dbaaad127199a6295d60c6.jpg', 'MIDDELEEUWEN', 'MITTELALTER', 'Koning en Koningin - Musketier - Ridder', 'König und Königin - Musketier - Ritter', 1, 60),
(77, '20075ef1f2ca19820bf7256df34dfd2c.jpg', 'SPORT', 'SPORT', 'Cheerleader - Bokser - Formule 1', 'Cheerleader - Bokser - Formule 1', 1, 53),
(78, '9bd5a23a73867b75cc040c30b9da0bd0.jpg', 'RELIGIE', 'RELIGION', 'Profeet - Non Monnik Non - Engel', 'Prophet - Nonne Mönch Nonne - Engel', 1, 26),
(79, '0baa71d162f5ae5dc75a7157a4bb157e.jpg', 'TONEEL', 'THEATER', 'Oud boertje - Oma ', 'alter Bauer - Großmutter', 1, 62),
(81, '2a310408629eaa2409459cf8f2cca3ec.jpg', 'FUN', 'SPAß', 'Mister glamour gold - Herft - Disco - Mister glamour silver', 'Mister glamour gold - Herbst - Disco - Mister glamour silver', 1, 61),
(82, 'a8bf729c4aa5768bd8e84f0e15f12367.jpg', 'DIEREN', 'TIERE', 'Slang vrouw - Slang man - Panter', 'Schlangenfrau - Schlangenmann - Panther', 1, 65),
(91, '793af74ec9ba97a95c0f6db9dc2cc33c.jpg', 'VRIJGEZELLEN PARTY', 'JUNGGESELLE PARTY', 'Vrijgezellen party', 'Junggeselle party', 1, 70),
(92, 'e236213b82c496e2fbb8bd001570a7fa.jpg', 'BRUILOFT KIDS', 'HOCHZEIT KINDER', 'Bruidegom Bruidsjonker - Bruidsmeisje', 'BrautJunge - Brautjungfer', 1, 81),
(85, 'f735804e81ef8583151fbee13c0816db.jpg', 'LANDEN', 'LÄNDER', 'Hollandse - Antje', 'Hollandse - Antje', 1, 51),
(86, '0c3d17ad52361fecf7ea366f514075ce.jpg', 'Brillen', 'Brille', 'Nerd glasses - Bierbril - Handboeibril ', 'Nerd glasses - Bierbrille - Handschelle Brille', 1, 66),
(90, '359ed27bcc833fb27084b30aeb5f0b89.jpg', 'LANDEN', 'LÄNDER', 'IJslandse - Waarzegger - Parijs', 'Isländisch - Wahrsager - Paris', 1, 69),
(88, 'c81f26e0edda9f25a9034811875c67be.jpg', 'TONEEL', 'THEATER', 'Boerenknecht - Zomers - Postbode - Zwerver', 'Bauern Knecht - Sommerlich - Briefträger - Wanderer', 1, 52),
(89, 'a8fb303c317c4fb0f4df7b3532f15854.jpg', 'METAL', 'METAL', 'Gothic - Gothic - Metal', 'Gothic - Gothic - Metal', 1, 47),
(93, '928687118e1478d69c8bb985fb9ea0af.jpg', 'FUN', 'SPAß', 'Regenboog vrouw - regenboog man - regenboog fee - Regenboogkostuum ', 'Regenbogen frau - Regenbogen mann - Regenbogen fee - RegenbogenKostüm', 1, 22),
(97, '412b8197ba44561c5df9b7644b8549b9.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Hippie - Oosterse prinses - Disco Girl', 'Hippie - Oriental Princess - Disco Girl', 1, 82),
(98, 'fc75d001ea52ec6f1c5df732ab0114f8.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Kasteel Heks - Spookje - Evil Witch', 'Schloss Hexe - Gespenst - Evil Witch', 1, 83),
(99, '5e2dcf7f955f32cb4581c049aacee8e4.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Obelix - Holbewoner - Zeemeermin', 'Obelix - Steinzeit - Meerjungfrau', 1, 84),
(100, '3864991d7c4ba2e2bd779d6fc55dacbd.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Piraten meisje - Priester - Engel', 'Piraten-Mädchen - Priester - Engel', 1, 87),
(101, '7473a7d619ef30b98cb89a8c75d9a6f1.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Magische Fee - Zeemeermin - Gevangene Meisje', 'Magische Fee - Meerjungfrau - Prisoner Mädchen', 1, 86),
(102, '6d0eb9f74d88880153d8edc278e30f10.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Hippie - Super Hero - Soldaat Commando ', 'Hippie - Super Hero - Soldat Befehl', 1, 85),
(103, '9176a503bb0454732c836f2c7a8d8efe.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Verpleegster - Ridder - Kasteel dame', 'Krankenschwester - Ritter - Schloss Lady', 1, 89),
(104, '209520b46c96414bce0b2f2b3825f5e5.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'Skelet - Fleurige heks - Kleine enge creatie', 'Skelett - Bunte Hexe - little scary creation', 1, 88),
(105, '97ba7a582384cde9413b1f6f8304348c.jpg', 'KINDERFEESTJE PIRATEN', 'KINDER-PARTY', 'De kinderen worden ontvangen in het hoofdkwartier waar ze worden verkleed en geschminkt als echte piraten.\r\nHet is een middag met spel, knutsel, spannende speurtocht en nog veel meer.\r\nBel of mail gerust voor meer informatie. \r\nTel: 0597855447  Email: jojofeestwinkel@hotmail.com', '', 1, 107),
(106, '399872f2d5b83e494713a322e448cea5.jpg', 'LANDEN', 'LÄNDER', 'Schotse man - Tiroler kleding', 'Scottish Mann - Tiroler Kostüme', 1, 24),
(107, 'a23330260012636fbede08c38f3dee9c.jpg', 'GALA JEUGD', 'GALA JUGEND', '', '', 1, 93),
(108, '1c2c729308f48fb9ed63faf98a3cfd56.jpg', 'WESTERN', 'WESTERN', '', '', 1, 95),
(109, '7811cafc88dbb68df704139655d838a5.jpg', 'TONEEL', 'THEATER', 'Uniformen\r\nPolitie kostuum - Marechaussee kostuum Kapitein kostuum', 'Uniformen', 1, 74),
(133, '0ccb172426c26526d7da8d50aca84f4e.jpg', 'GRIEZEL', 'GRUSELIG', 'Heksen kostuums', 'Hexen Kostüme', 1, 75),
(135, 'b39b1272963aea1e29c4942f035a4cc9.jpg', 'Abraham pop - Sarah pop', 'Abraham - Sarah - 50 Jahre', 'Pop op ware grote - Aankleden naar eigen keuze - Als Abraham / Sarah - Of voor een ander Jubilea of thema feest ', 'Pop voller großer - Kleiden Sie Ihre Wahl - Für Jubiläen oder andere Mottopartys', 1, 108),
(134, '813d1bcf2906691ac762b98ce609b73b.jpg', 'SWIEBERTJE ', 'SWIEBERTJE', 'Swiebertje kostuum - Saartje kostuum - Bromsnor kostuum - Malle Pietje kostuum - De burgermeester kostuum\r\n', 'Wanderer - Kellnerin - Dorfpolizist - Antique Kaufmann - Der Bürgermeister ... die guten alten Zeiten', 1, 105),
(131, '619de5f8dc0cbb742fd40884845712bf.jpg', 'FUN', 'SPAß', 'Super Boy - Smurf - Super Girl', 'Super boy - Schlümpfe - Super girl', 1, 104),
(110, '8f0759b3a1750b01d58381b928defec1.jpg', 'Paskamer ', 'Umkleidekabine', 'Paskamer ', 'Ankleideraum Präsenz', 1, 55),
(111, '5b6d606bf6c895abe84b80c8d96d1463.jpg', 'GRIEZEL', 'GRAUEN', 'Enge ogencape - weerwolf - Heks', 'Augen Cape - Werwolf - Hexe', 1, 64),
(112, 'a3b360deb8ea47908461a754e80cd67e.jpg', 'GRIEZEL', 'GRAUEN', 'Cape zwart - Beul - Duivel ', 'schwarzen Umhang - Scharfrichter - Teufel', 1, 68),
(113, '3329e6c94d042b577204f5d78ad37bbd.jpg', 'DIEREN', 'TIERE', 'Zwarte beer - Koe - Koe - Bruine beer', 'Schwarze bär - Kuh - Kuh - Braun bär', 1, 37),
(114, 'a5482fa70a90439b31c87c48eb091d91.jpg', 'BAROK', 'BAROK', 'Middeleeuws kostuums voor kinderen\r\nKinderen Theater kostuums\r\n', 'Mittelalterliche Kostüme für Kinder\r\nKinder-Theater Kostüme', 1, 100),
(115, '4a26f831ed601aaceceb180b29ea78b0.jpg', 'MIDDELEEUWEN', 'MITTELALTER', 'Venetiaanse edelman - Venetiaanse edelvrouw - Marcus de 1e', 'Venezianischer Edelmann - Venezianerin - Marcus de 1e', 1, 8),
(116, '5eefa31e5a8869dbaa3511780c1d0169.jpg', 'LANDEN', 'LÄNDER', 'Sherlock Holmes - Inuit - Samurai', 'Sherlock Holmes - Inuit - Samurai', 1, 72),
(117, 'ffe451e928e523772995915b841b2eff.jpg', 'stenen tijdperk', 'Steinzeit', 'Barney - Wilma en Fred - The Flintstones - Holbewoner', 'Barney - Wilma en Fred - The Flintstones - Neandertaler', 1, 73),
(118, '40850f1aaad9933cd3f5342dcad347aa.jpg', 'KINDERKOSTUUMS', 'KINDER-KOSTÜME', 'gevangenen - zorro', 'Gefangene - Zorro', 1, 90),
(119, 'e15f7dd5dc10f278b09a053d1592783b.jpg', 'TONEEL', 'THEATER', 'Jurken van toen', 'Kleider altmodisch', 1, 77),
(124, '4104763696caf3d634440b382174664a.jpg', 'AVONDJURKEN', 'Abendkleider', 'Avondjurken', 'Abendkleider', 1, 94),
(121, 'f391ff631c92243495ac73ea45456e00.jpg', 'WESTERN', 'WESTERN', 'Western cowboys', 'Western cowboys', 1, 76),
(125, '010f00a3f734a1ae6fc3bcde6b14548b.jpg', 'PIRATEN', 'PIRATEN', 'Kapitein - Piraten', 'Kapitän - Piraten', 1, 96),
(123, '10ddb2734f27155a9dc991e736a62ce5.jpg', 'GALA', 'GALA', 'Gala kostuums \r\nGala kleding voor alle leeftijden', 'Gala Kostüme\r\nAbendmode für alle Altersgruppen', 1, 91),
(126, 'ec5da57d2fe733187fee0b09ae788f76.jpg', 'KOSTUUMS KINDEREN', 'Kostüme Kinder', 'Nar - Koning - Tovernaar', 'Hofnarr - König - Zauberer', 1, 97),
(127, '7cf100428ec7c00248f2c77bcb7096c2.jpg', 'LANDEN', 'LÄNDER', 'Chinees kostuum - Oosters kostuum - Chinese dame ', 'Chinesisches Kostüm - Oriental Kostüm - Chinese lady', 1, 99),
(128, '494a4fb8aac374e570529c1a0c25d372.jpg', 'Dieren Kostuum', 'Dieren Kostüm', 'Draak kostuum - Kangoeroe kostuum - Rups kostuum', 'Drachen-Kostüm - Kangaroo Kostüm - Caterpillar-Kostüm', 1, 101),
(129, '7b2154a2f0a4b3388ec9fcda6a2e082c.jpg', 'Kinderen kostuums', 'Kinder Kostüme', 'Musketier kostuum - Zorro kostuum - Ridder kostuum', 'Musketier-Kostüm - Zorro-Kostüm - Ritter Kostüm', 1, 102),
(130, '18ce856d2539d9442a49b0518fac68fa.jpg', 'Griezel Kostuums', 'Horror Kostüme', 'Griezel Kostuums - Heksen kostuum', 'Horror Kostüme - Hexen-Kostüm', 1, 78),
(132, 'db6fdd845fa5aca42fcc401a3071bcde.jpg', 'DIEREN', 'TIERE', 'Leeuw - Draak - Hamster', 'Löwe - Drachen - Hamster', 1, 103),
(136, '29f89dbdb6c2e523f879d838a3c96804.jpg', 'DIEREN', 'TIERE', 'varken kostuum - witte uil kostuum - papegaai kostuum', 'Schwein Kostüm - weiße Eule Kostüm - Papagei Kostüm', 1, 106),
(137, 'e601153270caf8261d5cf292dd72f49b.jpg', 'SPROOKJES', 'Märchen', 'Minnie Mouse kostuum - Dopey kostuum  - sneeuwwitje kostuum - Kaboutertje kostuum ', 'Minnie Mouse kostuum - Seppl kostuum - Schneewittchen kostuum - Gnome Mädchen', 1, 59),
(138, '455dccb4a4d4f919913596e351480d32.jpg', 'Tiroler kostuums', 'Tiroler Kostüme', 'Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest \r\nTiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding', 'Oktoberfest - Bayern fest - Bierfest - Tiroler Kostüme', 1, 15),
(139, '30059ac33c2cbe136ab63343186e1903.jpg', 'Tiroler kostuums - Bierfeest -', 'Bayern Kostüme - Tiroler Kostü', 'Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest \r\nTiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding', 'Bayern Kostüme - Tiroler Kostüme - Bierfest Kostüme', 1, 17),
(140, '74c8af41e1bb3d19d59b739f77c3a38c.jpg', 'Tiroler kostuums - Bierfeest -', 'neues ThemaTiroler kostuums - ', 'Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest \r\nTiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding', 'Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest \r\nTiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding', 1, 13),
(141, '193550f170edbc66f887e1d3ac943b85.jpg', 'Tiroler kostuums - Bierfeest -', 'Tiroler Kostüme', 'Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest  Tiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding Maat 116 tot maat 66', 'Grosse 116 bis grosse 66 - Tiroler kostuums - Bierfest - Oktoberfest - Beierse fest  Tiroler jurkjes - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding', 1, 49),
(142, 'fbf5c4a85acc9f471864b9d31480ab90.jpg', 'Hippe kleding', 'Hippe Kostüme', 'Seventies kleding - seventies kostuums', 'Seventies Kostüme ', 1, 21),
(143, '39eacdd274fb51957a1b5536576b98bf.jpg', 'zaterdag', 'samstag 10 januar ab 14:00 uur', 'zaterdag', 'samstag 10 januar ab 14:00 uur geschlossen', 0, 3),
(144, '1155309aec3530f0e286e9dd3620673a.jpg', 'KERSTMAN LUXE € 30,00 per dag ', 'WEIHNACHTSMANN € 30,00 pro tag', 'kerstmannen kostuums, kerstman hulpje en ook mooie engelen kostuums zijn er aanwezig', 'WEIHNACHTSMANN KOSTÜME, Weihnachtsmann-Helfer, Engel Kostüme', 0, 2),
(145, '6edd9bd8267cbdcc32fc7d4083c7031e.jpg', 'paashaas-kostuum', 'Osterhase-Kostüm', 'Paashaas - kostuum ', 'Osterhase-Kostüm ', 1, 6),
(147, '319019eed83de9f2f4a1738151122ac0.jpg', 'Tiroler kostuums - Bierfeest -', 'Tiroler Kostüme - Oktoberfest', 'Tiroler jurkjes - Tiroler kostuums - Bierfeest - Oktoberfeest - Beierse feest  Tirolerjurkje - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding - Bayern Kostüme', 'Tiroler Kostüme - Bierfeest - Oktoberfeest - Beierse feest  Tirolerjurkje - Tiroler broeken - Lederhosen - oktoberfest - Tiroler kleding - Bayern Kostüme', 1, 9),
(146, '375d53db763d0225ca5ef8d2c157a8dc.jpg', 'Paashaas kostuum ', 'Osterhase-Kostüm', 'paashaas kostuum huren ', 'osterhase ', 1, 19),
(148, 'e5d309c145add6a9098cdf0ed791e201.jpg', 'LANDEN', 'LÄNDER', 'Delftsblauw jurkje met kapje - Tulpen kostuum met kapje - Delftsblauw kostuum heren met pet ', 'Antje kleid mit kopfteil - Antje tulpekostume mit kopfteil Herren kostuum Delfsblauw', 1, 11),
(149, 'd20433cdd7a6a8575c623f20b44863a2.jpg', 'Hippie', 'Hippie', 'Hippie catsuit - Flowerpower - Hippie Kostuum man', 'Hippie catsuit - Flowerpower - Hippie Kostuum Herren', 1, 14),
(150, '778e64fecb70d6d351e68df981a57dff.jpg', 'FUN - Hippie - katy P.', 'FUN - Hippie - katy P.', 'Hippie Heren - Katy P. - Perry jurkje - Hippie fun', 'Hippie Heren - Katy P. - Perry kleid - Hippie fun', 1, 12),
(151, '1a510e7726c9264042cfc2055afac46e.jpg', 'HELDEN ', 'HELDEN', 'Super Mario kostuum - Asterix kostuum - Zorro Kostuum - Helden kostuum', 'Super mario kostüm - Asterix kostüm - Zorro kostüm - Superhero kostume ', 1, 35),
(152, '20e8841666105d3a2dcf27039113f119.jpg', 'LANDEN', 'LÄNDER', 'Landen - Ik hou van Holland kostuum', 'Länder kostüme', 1, 63),
(153, 'e039672e367bffc360399d28d9f40143.jpg', '50, 60, en 70-ties', '50, 60, en 70er Jahre', 'sixties jurkjes - jimi hendrix kostuum - seventies kostuums - seventies jurkjes', '60ties kleid - jimi hendrix kostume - 70ties kostuums - 70ties kleid', 1, 67);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

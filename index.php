<?php
//if (isset($_GET["pw"])) {
//  $pw = $_GET["pw"];
//  if ($pw == "19691986") {
try {

    if (isset($_GET["page"])) {
        switch (strtolower($_GET["page"])) {
            case "home":
                $pageName = $_GET["page"];
                $pageTitle = "";
                break;

            case "themes":
                $pageName = $_GET["page"];
                $pageTitle = "Thema's";
                break;

            case "pricesandconditions":
                $pageName = $_GET["page"];
                $pageTitle = "Verhuurprijzen en -condities";
                break;

            case "events":
                $pageName = $_GET["page"];
                $pageTitle = "Events";
                break;

            case "directions":
                $pageName = $_GET["page"];
                $pageTitle = "Directions";
                break;

            default:
                $pageName = "Home";
                $pageTitle = "";
                //                  require 'altPage.html';
                break;
        }
        if (isset($pageName)) {
            include 'modules/JOJOPage.php';
            if (strlen($pageTitle) > 0) {
                $pageTitle .= " - ";
            }
            $page = new JOJOPage(strtolower($pageName), $pageTitle . "Feestbedrijf JOJO Vriescheloo");
            $pageName = ucfirst($pageName);
            $page->draw($pageName);
        } else {
            require 'altPage.html';
            exit();
        }
    } else {

        $pageName = "Home";
        $pageTitle = "";

        include 'modules/JOJOPage.php';
        $page = new JOJOPage(strtolower($pageName), $pageTitle . "Feestbedrijf JOJO Vriescheloo");
        $pageName = ucfirst($pageName);
        $page->draw($pageName);
    }
} catch (Exception $e) {
    echo ("<style>body{font-family:sans-serif}h2{color:#800}a{color:blue}h3,h4,h5,h6,pre{margin:2px;padding:0}h3{background:#800;color:#fff;padding:4px}pre{font-size:10px}</style><h1>Whoopsie!</h1><h2>Kim broke the site!</h2><p>You can write him angry emails at kimhogeling[at]gmail.com<br><b>Don't forget to copy URL (the full address of the current page)</b></p><p><a href='/'>Click here to get back to start</a></p>");
    if (isset($_SERVER['HTTP_KIMDEBUG'])) {
        echo '<h3>' . $e->getMessage() . '</h3>';
        echo '<h4>' . $e->getFile() . ':' . $e->getLine() . '</h4>';
        $trace = $e->getTrace();
        foreach ($trace as $key => $tr) {
            echo '<h5>' . $tr['file'] . ':' . $tr['line'] . '</h5>';
            echo '<h6> class ' . $tr['class'] . '. function ' . $tr['function'] . '</h6>';
            echo '<pre>';
            var_dump($tr['args']);
            echo '</pre>';
        }
    }
}
?>
<?php

class html {

    private $doctype;
    private $head;
    private $body;
    private $lang;
    private $config;

    function __construct($lang = 'NL') {
        $this->doctype = '<!DOCTYPE html>';
        $this->head = '';
        $this->body = '';
        $this->lang = $lang;
        include 'config.php';
        $this->config = $config;
        dibi::connect($config['database']);
    }

    /**
     * deploy the html document
     */
    public function deploy() {
        echo ($this->doctype . $this->head . $this->body);
    }

    /**
     * start or end head
     * @param type $start (true for opentag, false for closetag)
     */
    public function html($start = true) {
        if ($start) {
            $this->addToHead('<html>');
        } else {
            $this->addToBody('</html>');
        }
    }

    /**
     * start or end head
     * @param type $start (true for opentag, false for closetag)
     */
    public function head($start = true) {
        if ($start) {
            $this->addToHead('<head>');
        } else {
            $this->addToHead('</head>');
        }
    }

    /**
     * title of htmldocument
     * @param type $title
     */
    public function title($title) {
        if (strlen($title) > 0) {
            $this->addToHead("<title>$title</title>");
        }
    }

    /**
     * set charset
     * @param type $code (utf8, ...)
     */
    public function charset($code) {
        if ($code == "utf8") {
            $this->addToHead('<meta charset="utf-8" />');
        }
    }

    public function preventMobileScaling() {
        $this->addToHead('<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0">');
    }

    public function csslink($filename) {
        if (file_exists($filename)) {
            $this->addToHead('<link rel="stylesheet" type="text/css" href="' . $filename . '">');
        } else {
            echo $filename . " Could not find file: error";
            die();
        }
    }

    public function favicon($filename = 'favicon') {
        $this->addToHead('<link rel="icon" href="' . $filename . '.ico" type="image/x-icon">');
    }

    public function cssGoogleWebFont($family) {
        $this->addToHead('<link href="http://fonts.googleapis.com/css?family=' . $family . '" rel="stylesheet" type="text/css">');
    }

    public function jslink($filename) {
        if (file_exists($filename)) {
            $this->addToBody('<script async src="' . $filename . '" ></script>');
        } else {
            echo $filename . " Could not find file: error";
            die();
        }
    }

    /**
     * start or end body
     * @param type $start (true for opentag, false for closetag)
     */
    public function body($start = true) {
        if ($start) {
            $this->addToBody('<body>');
        } else {
            $this->addToBody('</body>');
        }
    }

    /**
     * start or end top
     * @param type $start (true for opentag, false for closetag)
     */
    public function top($start = true) {
        if ($start) {
            $this->addToBody('<div id="top">');
        } else {
            $this->addToBody('</div>');
        }
    }

    /**
     * start or end top
     * @param type $start (true for opentag, false for closetag)
     */
    public function mainwrapper($start = true) {
        if ($start) {
            $this->addToBody('<div id="mainwrapper">');
        } else {
            $this->addToBody('</div>');
        }
    }

    public function fieldset($attributs = array()) {
        $fieldset = '<fieldset ';
        foreach ($attributs as $attribut => $attvalue) {
            if ($attribut == "id" OR $attribut == "class") {
                $fieldset .= $attribut . '="' . $attvalue . '" ';
            }
        }
        $fieldset .= ">";
        if (isset($attributs["legend"])) {
            if (strlen($attributs["legend"]) > 0) {
                $fieldset .= "<legend>" . htmlspecialchars($attributs["legend"]) . "</legend>";
            }
        }
        if (isset($attributs["html"])) {
            if (strlen($attributs["html"]) > 0) {
                $fieldset .= htmlspecialchars($attributs["html"]);
            }
        }
        $fieldset .= "</fieldset>";
        $this->addToBody($fieldset);
    }

    /**
     * Create the logo (only one is allowed)
     * @param string $imgurl (referes to /media/images/)
     * @param int $width
     * @param int $height
     * @param string $title
     */
    public function logo($imgurl = "shopPhoto.jpg", $title = "title") {
        $this->addToBody('<a href="/" id="logo"><img class="logoimg" src="media/images/' . $imgurl . '" alt="' . $title . '"></a>');
    }

    /**
     * Create the contact info(only one)
     */
    public function contactAndOpened() {
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", 'contact'),
        ));
        $contact = '<div id="telephonenumberwrapper" class="shadow">';
        $contact .= $result->fetchSingle();
        $contact .= '</div>';

        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", 'opened'),
        ));
        $opened = '<div id="openedwrapper" class="rotatebackwards">';
        $opened .= $result->fetchSingle();
        $opened .= '</div>';

        $opentext = $this->lang === 'DE' ? 'Öffnungszeiten & Kontakt' : 'Openingstijden & contact';
        $openbutton = '<div id="shownotes">' . $opentext . '</div>';

        $this->addToBody($openbutton . '<div id="noteswrapper" class="cf">' . $contact . $opened . '</div>');
    }

    /**
     * Create the menu (only one is allowed)
     * @param array $md ("titles" => "urls")
     */
    public function menu() {
        $menu = '<div id="menuwrapper" class="cf">';
        $lang = $this->lang;
        $md = $this->config["menu"][$lang];
        $menu .= '<a class="menuitem" id="menu1" href="' . $md[0]["url"] . '">' . $md[0]["title"] . '</a>';
        $menu .= '<a class="menuitem" id="menu2" href="' . $md[1]["url"] . '">' . $md[1]["title"] . '</a>';
        $menu .= '<a class="menuitem" id="menu3" href="' . $md[2]["url"] . '">' . $md[2]["title"] . '</a>';
        $menu .= '<a class="menuitem big" id="menu4" href="' . $md[3]["url"] . '">' . $md[3]["title"] . '</a>';
        $menu .= '<a class="menuitem" id="menu5" href="' . $md[4]["url"] . '" target="_blank">' . $md[4]["title"] . '</a>';
        $menu .= '</div>';
        $this->addToBody($menu);
    }

    /**
     * start or end middle
     * @param type $start (true for opentag, false for closetag)
     */
    public function middle($start = true) {
        if ($start) {
            $this->addToBody('<div id="middle" class="cf">');
        } else {
            $this->addToBody('</div>');
        }
    }

    /**
     * Create a main image
     * @param string $imgurl (referes to /media/images/)
     * @param int $width
     * @param int $height
     * @param string $title
     */
    public function mainImage($imgurl = "stiftungwarentest.gif", $title = "title") {
        $mainimage = '<div class="mainimagewrapper">';
        $mainimage .= '<img class="shadow mainimg" src="media/images/' . $imgurl . '" alt="' . $title . '">';
        $mainimage .= '</div>';
        $this->addToBody($mainimage);
    }

    public function getTrans($key) {
        return "have to translate: " . $key;
    }

    public function themes() {
        $result = dibi::query('SELECT * FROM themes WHERE active = %i ORDER BY sequence ASC', 1);
        $all = $result->fetchAll();
        $lang = $this->lang;
        $id = false;
        if (isset($_GET["theme"])) {
            $theme = $_GET["theme"];
            if (filter_var($theme, FILTER_VALIDATE_INT)) {
                $result = dibi::query('SELECT * FROM themes WHERE %and', array(
                    array("`active` = %i", 1),
                    array("`id` = %i", $theme),
                )
                );
                $result = $result->fetchAll();
                $id = $result[0]["id"];
                $photo = $result[0]["photo"];
                $title = $result[0]["name" . $this->lang];
                $description = $result[0]["description" . $this->lang];
            }
        }
        if ($id) {
            $html = '<div id="bigthemewrapper">';

            $arrows = '';

            $currentSeq = $result[0]["sequence"];

            $asdf = dibi::query("SELECT MIN(sequence) FROM themes WHERE active=1");
            $minSequence = $asdf->fetchSingle();
            $asdf = dibi::query("SELECT MAX(sequence) FROM themes WHERE active=1");
            $maxSequence = $asdf->fetchSingle();

            if ($currentSeq != $minSequence) {
                $cur = $currentSeq; //moet omdat currentSeq ook bij de pijl naar rechts wordt gebruikt.
                $prevId = false;
                while (!$prevId) {
                    $asdf = dibi::query("SELECT id FROM themes WHERE %and", array(
                        array("sequence=%i", $cur - 1),
                        array("active=1"),
                    ));
                    $prevId = $asdf->fetchSingle();
                    $cur--;
                }

                $arrows .= '<div id="arrowleft"><a id="linkleft" href="?page=themes&theme=' . $prevId . '">&lt;</a></div>';
            }

            if ($currentSeq != $maxSequence) {
                $nextId = false;
                while (!$nextId) {
                    $asdf = dibi::query("SELECT id FROM themes WHERE %and", array(
                        array("sequence=%i", $currentSeq + 1),
                        array("active=1"),
                    ));
                    $nextId = $asdf->fetchSingle();
                    $currentSeq++;
                }
                $arrows .= '<div id="arrowright"><a id="linkright" href="?page=themes&theme=' . $nextId . '">&gt;</a></div>';
            }

//          $html .= "<div><pre>" . print_r($result, true) . "</pre></div>";
            //          $html .= "<div style='color: black; background-color: white;'>current: " . $result[0]["sequence"] . ". min: " . $minSequence . ". max: " . $maxSequence . "</div>";

            $html .= $arrows;

            $html .= '<h1 class="glow sheettitle">' . $title . '</h1>';
            $html .= '<img class="rotate glowbox bigthemeimg" src="media/images/themes/';
            $html .= $photo;
            $html .= '" alt="' . $title . '"><p class="glow">';
            $html .= $description;
            $html .= "</p></div>";
        } else {
            $html = '';
            foreach ($all as $theme) {
                $html .= '<a class="themewrapper" href="?page=themes&theme=' . $theme["id"] . '" title="' . $theme["name" . $lang] . '">';
                $html .= '<img src="media/images/themes/small_' . $theme["photo"] . '" alt="' . $theme["name" . $lang] . '"><br>';
                $html .= '<span>' . $theme["name" . $lang] . '</span>';
                $html .= "</a>";
            }
        }
        $this->addToBody($html);
    }

    public function pricelist() {
        $pricelist = '<div id="sheet" class="shadow">';
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", 'pricelisttitle'),
        )
        );
        $title = $result->fetchSingle();
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", "pricelisttext"),
        )
        );
        $text = $result->fetchSingle();
        $pricelist .= '<h1 class="sheettitle">' . $title . '</h1>';
        $pricelist .= '<p>' . $text . '</p>';
        $pricelist .= '</div>';
        $this->addToBody($pricelist);
    }

    public function events() {
        $events = '<div id="sheet" class="shadow">';
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", 'eventstitle'),
        )
        );
        $title = $result->fetchSingle();
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", "eventstext"),
        )
        );
        $text = $result->fetchSingle();
        $events .= '<h1 class="sheettitle">' . $title . '</h1>';
        $events .= '<p>' . $text . '</p>';
        $events .= '</div>';
        $this->addToBody($events);
    }

    public function directions() {
        $directions = '<div id="sheet" class="shadow directions" style="text-align:center">';
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", 'directionstitle'),
        )
        );
        $title = $result->fetchSingle();
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", $this->lang),
            array("`key` = %s", "directionstext"),
        )
        );
        $text = $result->fetchSingle();
        $directions .= '<h1 class="sheettitle">' . $title . '</h1>';
        $directions .= '<p>' . $text . '</p>';
        $directions .= '</div>';
        $this->addToBody($directions);
    }

    public function logout() {
        $logout = '<a title="afmelden" id="logout" href="admin.php?logout">';
        $logout .= '<img src="media/images/actions/logout.png" width="48" height="48">';
        $logout .= '</a>';
        $this->addToBody($logout);
    }

    public function adminTop() {
        $top = '<div id="top"><h1 class="rotate">JOJO BEHEER</h1></div>';
        $this->addToBody($top);
    }

    public function adminStart() {
        $start = '<div id="adminmenu" class="shadow"><h2>Hoi Mam! Wat wilt je doen?</h2>';
        $md = $this->config["adminmenu"];
        $start .= "<ul>";
        foreach ($md as $key => $item) {
            $start .= '<li><a href="' . $item["url"] . '">' . $item["title"] . '</a></li>';
        }
        $start .= "</ul></div>";
        $this->addToBody($start);
    }

    public function adminThemes() {
        $themes = '<div id="themes">';
        $result = dibi::query('SELECT * FROM themes ORDER BY sequence ASC');
        $all = $result->fetchAll();

        $themes .= "<table>";
        $themes .= '<tr><th class="invisible">&nbsp;</th><th colspan="2">Naam</th><th colspan="2">Beschrijving</th><th class="invisible"></th></tr>';

        $themes .= "<tr><th width=\"100\">Foto</th><th>NL<th>DE</th><th>NL<th>DE</th><th width=\"240\">acties</th></tr>";
        foreach ($all as $theme) {
            if ($theme["active"] == "1") {
                $trmarker = '';
            } else {
                $trmarker = ' class="inactivetheme"';
            }
            $themes .= "<tr$trmarker>";
            $themes .= '<td><a name="' . $theme["id"] . '" target="_blank" href="index.php?page=themes&theme=' . $theme["id"] . '&timestmp=' . time() . '"><img src="media/images/themes/small_' . $theme["photo"] . '" width="' . $this->config["themes"]["smaller"]["width"] . '" height="' . $this->config["themes"]["smaller"]["height"] . '"></a>';
            $themes .= '</td><td>' . $theme["nameNL"];
            $themes .= '</td><td>' . $theme["nameDE"];
            $themes .= '</td><td>' . substr($theme["descriptionNL"], 0, 200) . '';
            $themes .= '</td><td>' . substr($theme["descriptionDE"], 0, 200) . '';
            $themes .= "</td>";
            $themes .= "<td>";
            $themes .= '<a title="aanpassen" class="rotatable" href="admin.php?page=Themes&edit=' . $theme["id"] . '&timestmp=' . time() . '"><img src="media/images/actions/edit.png" width="48" height="48"></a>';

            $result = dibi::query("SELECT MIN(sequence) FROM themes");
            $minSequence = $result->fetchSingle();

            if ($theme["sequence"] != $minSequence) {
                $themes .= '<a href="admin.php?page=Themes&up=' . $theme["id"];
                $themes .= '&tmstamp=' . time() . '#' . $theme["id"];
                $themes .= '"><img src="media/images/actions/up.png" width="48" height="48"></a>';
            } else {
                $themes .= '<img src="media/images/actions/up_inactive.png" width="48" height="48">';
            }

            $result = dibi::query("SELECT MAX(sequence) FROM themes");
            $maxSequence = $result->fetchSingle();

            if ($theme["sequence"] != $maxSequence) {
                $themes .= '<a href="admin.php?page=Themes&down=' . $theme["id"];
                $themes .= '&tmstamp=' . time() . '#' . $theme["id"];
                $themes .= '"><img src="media/images/actions/down.png" width="48" height="48"></a>';
            } else {
                $themes .= '<img src="media/images/actions/down_inactive.png" width="48" height="48">';
            }

            if ($theme["active"] == 0) {
                $switch = "on";
                $title = "inschakelen";
            } else {
                $switch = "off";
                $title = "uitschakelen";
            }
            $themes .= '<a title="' . $title . '" class="rotatable" onclick="if (confirm(\'Thema ' . $title . '?\n\n' . $theme["nameNL"] . '\')) { document.location = \'admin.php?page=Themes&turn' . $switch . '=' . $theme["id"] . '&timestmp=' . time() . '#' . $theme["id"] . '\' }" href="#"><img src="media/images/actions/turn' . $switch . '.png" width="48" height="48"></a>';
            if ($theme["active"] == 0) {
                $themes .= '<a title="verwijderen" class="rotatable" onclick="if (confirm(\'Thema verwijderen?\n\n' . $theme["nameNL"] . '\')) { document.location = \'admin.php?page=Themes&delete=' . $theme["id"] . '&timestmp=' . time() . '\' }" href="#"><img src="media/images/actions/delete.png" width="48" height="48"></a>';
            }
            $themes .= "</td></tr>";
        }
        $themes .= '</table><a id="backToThemes" title="terug" class="rotatable" href="admin.php?timestmp=' . time() . '#' . $id . '"><img src="media/images/actions/back.png" width="64" height="64"></a><a id="newTheme" title="nieuw thema" class="rotatable" href="admin.php?page=Themes&new&tmstamp=' . time() . '"><img src="media/images/actions/new.png" width="64" height="64"></a></div>';
        $this->addToBody($themes);
    }

    public function editTheme($id) {
        $theme = '<a id="backToThemes" title="terug" class="rotatable" href="admin.php?page=Themes&timestmp=' . time() . '"><img src="media/images/actions/back.png" width="64" height="64"></a>';
        $result = dibi::query("SELECT * FROM themes WHERE id=%i", $id);
        $all = $result->fetchAll();
        $all = $all[0];
        $theme .= '<div id="sheet" >';
        $theme .= '
        <form enctype="multipart/form-data" action="" method="post">
<input type="hidden" name="MAX_FILE_SIZE"
value="5000000">
<h3>Foto:</h3>
<img src="media/images/themes/small_' . $all["photo"] . '" width="' . $this->config["themes"]["small"]["width"] . '" height="' . $this->config["themes"]["small"]["height"] . '"><br>
<label>kiezen:</label>
<input name="file" type="file" id="file">
<input type="submit" name="submit" value="Foto uploaden">
</form>';

        $theme .= '<form name="input" action="" method="post">';
        $theme .= "<h3>Naam:</h3>";
        $theme .= '<label>NL</label><input length="30" name="nameNL" type="text" value="' . $all["nameNL"] . '" /><br>';
        $theme .= '<label>DE</label><input length="30" name="nameDE" type="text" value="' . $all["nameDE"] . '" /><br>';
        $theme .= "<h3>Beschrijving:</h3>";
        $theme .= '<label>NL</label><textarea name="descNL">' . $all["descriptionNL"] . '</textarea><br><br>';
        $theme .= '<label>DE</label><textarea name="descDE">' . $all["descriptionDE"] . '</textarea><br>';
        $theme .= '<input type="submit" value="Thema opslaan" id="savebtn" />';
        $theme .= '</form>';
        $theme .= '</div>';
        $this->addToBody($theme);
    }

    public function adminEvents() {
        $events = '<a id="backToThemes" title="Terug naar begin" class="rotatable" href="admin.php"><img src="media/images/actions/back.png" width="64" height="64"></a>';
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", 'NL'),
            array("`key` = %s", 'eventstext'),
        )
        );
        $eventsNL = $result->fetchSingle();
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", 'DE'),
            array("`key` = %s", 'eventstext'),
        )
        );
        $eventsDE = $result->fetchSingle();
        $events .= '<div id="sheet" >';
        $events .= '<form name="input" action="" method="post">';
        $events .= "<h3>Activiteiten:</h3>";
        $events .= '<label>NL</label><textarea class="big" name="eventsNL">' . $eventsNL . '</textarea><br><br>';
        $events .= '<label>DE</label><textarea class="big" name="eventsDE">' . $eventsDE . '</textarea><br>';
        $events .= '<input type="submit" value="Opslaan" id="savebtn" />';
        $events .= '</form>';
        $events .= '</div>';
        $this->addToBody($events);
    }

    public function adminPrices() {
        $prices = '<a id="backToThemes" title="nieuw thema" class="rotatable" href="admin.php"><img src="media/images/actions/back.png" width="64" height="64" /></a>';
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", 'NL'),
            array("`key` = %s", 'pricelisttext'),
        )
        );
        $pricelistNL = $result->fetchSingle();
        $result = dibi::query("SELECT `translation` FROM `i18n` WHERE %and", array(
            array("`countrycode` = %s", 'DE'),
            array("`key` = %s", 'pricelisttext'),
        )
        );
        $pricelistDE = $result->fetchSingle();
        $prices .= '<div id="sheet" >';
        $prices .= '<form name="input" action="" method="post">';
        $prices .= "<h3>Prijslijst en verhuurcondities:</h3>";
        $prices .= '<label>NL</label><textarea class="big" name="pricelistNL">' . $pricelistNL . '</textarea><br /><br />';
        $prices .= '<label>DE</label><textarea class="big" name="pricelistDE">' . $pricelistDE . '</textarea><br />';
        $prices .= '<input type="submit" value="Opslaan" id="savebtn" />';
        $prices .= '</form>';
        $prices .= '</div>';
        $this->addToBody($prices);
    }

    private function addToHead($html) {
        $this->head .= $html;
    }

    private function addToBody($html) {
        $this->body .= $html;
    }

}

?>
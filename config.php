<?php

$config = array(
    'activemodules' => array(
        'html',
        'dibi/dibi',
    ),
    'activecss' => array(
        'jojo.min',
    ),
    'activejs' => array(
        'jojo.min',
    ),
    'admincss' => array(
        'admin',
    ),
    'adminjs' => array(
        'admin',
    ),
    'database' => array(
        'driver' => 'mysql',
        'host' => 'localhost',
        'username' => 'wepping_jojo',
        'password' => 'KIMSALABIM210786',
        'database' => 'wepping_jojo',
        'charset' => 'utf8',
    ),
    'menu' => array(
        'NL' => array(
            '0' => array(
                'title' => 'Kostuums',
                'url' => '?page=Themes',
            ),
            '1' => array(
                'title' => 'Activiteiten',
                'url' => '?page=Events',
            ),
            '2' => array(
                'title' => 'Route',
                'url' => '?page=Directions',
            ),
            '3' => array(
                'title' => 'Prijslijst & Verhuurcondities',
                'url' => '?page=PricesAndConditions',
            ),
            '4' => array(
                'title' => 'Blog',
                'url' => 'http://jojosfeestwinkel.blog.com',
            ),
        ),
        'DE' => array(
            '0' => array(
                'title' => 'Kostüme',
                'url' => '?page=Themes',
            ),
            '1' => array(
                'title' => 'Aktivitäten',
                'url' => '?page=Events',
            ),
            '2' => array(
                'title' => 'Anfahrt',
                'url' => '?page=Directions',
            ),
            '3' => array(
                'title' => 'Preisliste & Verleihbedingungen',
                'url' => '?page=PricesAndConditions',
            ),
            '4' => array(
                'title' => 'Blog',
                'url' => 'http://jojosfeestwinkel.blog.com',
            ),
        ),
    ),
    'themes' => array(
        'big' => array(
            'width' => 800,
            'height' => 400,
        ),
        'small' => array(
            'width' => 200,
            'height' => 130,
        ),
        'smaller' => array(
            'width' => 100,
            'height' => 60,
        ),
    ),
    'adminmenu' => array(
        '0' => array(
            'title' => 'Thema\'s beheren',
            'url' => '?page=Themes',
        ),
        '1' => array(
            'title' => 'Prijslijst & Voorwaarden aanpassen',
            'url' => '?page=PricesAndConditions',
        ),
        '2' => array(
            'title' => 'Contact aanpassen',
            'url' => '?page=Contact',
        ),
    ),

    // DEV overwrite:
    // 'database' => array(
    //     'driver' => 'mysql',
    //     'host' => 'localhost',
    //     'username' => 'root',
    //     'password' => 'root',
    //     'database' => 'wepping_jojo',
    //     'charset' => 'utf8',
    // ),
    // 'activecss' => array(
    //     'jojo',
    // ),
    // 'activejs' => array(
    //     'jojo',
    // ),

);
?>
<?php

class JOJOPage {

    private $html;
    private $config;

    public function __construct($page = "home", $title = "home") {
        require 'config.php';
        $this->config = $config;
        $this->head($page, $title);
        $this->openPage();
    }

    private function head($page = "home", $title = "home") {
        foreach ($this->config["activemodules"] as $module) {
            $filename = 'modules/' . $module . '.php';
            require $filename;
        }

        $lang = 'NL';

        if (isset($_GET['lang'])) {
            if ($_GET['lang'] == "DE") {
                $lang = 'DE';
            }
        } elseif (stripos(strtolower($_SERVER["HTTP_ACCEPT_LANGUAGE"]), "de") === 0) {
            $lang = 'DE';
        }

        $html = new html($lang);
        $html->html(true);
        $html->head(true);

        $html->cssGoogleWebFont('Acme');

        $html->favicon();

        foreach ($this->config["activecss"] as $css) {
            $cssfile = 'css/' . $css . '.css';
            if (file_exists($cssfile)) {
                $html->csslink($cssfile);
            } else {
                echo $cssfile . " does not exist! Cannot load style!";
                die();
            }
        }

        $html->charset("utf8");
        $html->preventMobileScaling();
        $html->title($title);

        $html->head(false);
        $this->html = $html;
    }

    private function openPage() {
        $html = $this->html;
        $html->body(true);
        $html->mainwrapper(true);
        $html->top(true);
        $html->logo('JOJOLogo.png', 'Feestbedrijf JOJO Vriescheloo');
        $html->contactAndOpened();
        $html->menu();
        $html->top(false);
        $html->middle(true);
    }

    public function draw($pageName = "home") {
        if (method_exists($this, "draw" . $pageName)) {
            $pageName = "draw" . $pageName;
            $this->$pageName();
        } else {
            die("don't know method: " . $pageName());
        }
        $this->closePage();
    }

    public function drawHome() {
        $html = $this->html;
        $html->mainImage('shopPhoto.jpg', "Shop Photo");
        $html->mainImage('horror/klaarhorror1.JPG', "Horror 3");
        $html->mainImage('horror/klaarhorror5.JPG', "Nieuw Horror");
        $html->mainImage('horror/griezelruimte3.jpg', "Horror 2");
    }

    public function drawThemes() {
        $html = $this->html;
        $html->themes();
    }

    public function drawPricesandconditions() {
        $html = $this->html;
        $html->pricelist();
    }

    public function drawDirections() {
        $html = $this->html;
        $html->directions();
    }

    public function drawEvents() {
        $html = $this->html;
        $html->events();
    }

    private function closePage() {
        $html = $this->html;
        foreach ($this->config["activejs"] as $js) {
            $jsfile = 'js/' . $js . '.js';
            if (file_exists($jsfile)) {
                $html->jslink($jsfile);
            } else {
                echo $jsfile . " does not exist! Cannot load style!";
                die();
            }
        }
        $html->middle(false);
        $html->mainwrapper(false);
        $html->body(false);
        $html->html(false);
        $html->deploy();
    }

}

?>
<?php

if (isset($_GET["logout"])) {
    $_SERVER['PHP_AUTH_USER'] = "";
    $_SERVER['PHP_AUTH_PW'] = "";
}

function authentifizieren() {
    header('WWW-Authenticate: Basic realm="JOJO Beheer"');
    header('HTTP/1.0 401 Unauthorized');
}

if ($_SERVER['PHP_AUTH_USER'] != "wepping" || $_SERVER['PHP_AUTH_PW'] != "KaasUitHetVuistje") {
    authentifizieren();
} else {
    switch (strtolower($_GET["page"])) {
        case "themes":
            $pageName = $_GET["page"];
            $pageTitle = "Thema's";
            break;

        case "pricesandconditions":
            $pageName = $_GET["page"];
            $pageTitle = "Verhuurprijzen en -condities";
            break;

        case "events":
            $pageName = $_GET["page"];
            $pageTitle = "Events";
            break;

        default:
            $pageName = "Start";
            $pageTitle = "Start";
            break;
    }
    if (isset($pageName)) {
        include 'modules/ADMINPage.php';
        $page = new ADMINPage(strtolower($pageName), $pageTitle . " - Beheer JOJO");
        $pageName = ucfirst($pageName);
        $page->draw($pageName);
    }
}
?>